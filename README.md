# Hoteles "Dalic" CICLO 3 P21 Grupo 2

***
La Aplicación de turismo "Dalic" es una aplicación web para mejorar los servicios de turismo. A través de la cual espera dar a conocer los hoteles más atractivos del Valle del cauca,  y pueda disfrutar la estadía en algún hotel de la región.


## Technologies
***
* [Python]
* [Framework django]
* [Html]
* [Css]
* [JavaScript]
* [Framework vue JS]

## Installation
***
* $ git clone https://damarishernandez@bitbucket.org/Damaris_Hernandez10/repositoriogrupo2.git
* $ Dirección de la Carpeta cd ../path/to/the/file

## Collaborateur
***
* DAMARIS HERNANDEZ
